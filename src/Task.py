#------------------------------------------------
#+++++++++++++Measurement.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      26.11.2020
#------------------------------------------------

from datetime import datetime
from PySide2.QtCore import Signal, QThread



#Top class of all topLevel task items. Each measurement and each task is an own object with a set of signals.
class Task(QThread):
    taskDone        = Signal( object )  #Current task finished
    statusUpdate    = Signal( object )  #The status of the device changed
    progressUpdate  = Signal( object )  #There is progress on the current task
    logUpdate       = Signal( object )  #New logs are available

    askPermission   = Signal( object, object ) #Using GUI related thing in QThreads leads to segementation faults

    CGREEN = '\033[32m'     #For green prints
    CRED   = '\033[91m'     #For red prints
    CEND   = '\033[0m'      #End color prints

    def __init__( self, pIndex, pType, pWait = False ):
        super(Task, self).__init__(  )
        self.index = pIndex
        self.type = pType

        self.status         = "Not running"
        self.task           = pType
        self.progress       = 0

        #Is set by the GUIController to keep track which tasks are already done
        self.aborted        = False
        self.running        = False
        self.permission     = False
        self.done           = False
        self.waitForTask    = pWait


    def taskFinished( self ):
        self.done = True
        self.running = False
        self.setProgress( 100 )
        if self.aborted:
            self.setStatus("Done: Aborted")
        else:
            self.setStatus("Done")
        self.taskDone.emit( self )

    def setStatus( self, pStatus ):
        self.status = pStatus
        self.statusUpdate.emit ( self )

    def setProgress( self, pProgress ):
        self.progress = pProgress
        self.progressUpdate.emit ( self )


    def stopTask ( self ):
        pass


#-----------------------------------------------------------------------
#------------------------ TASK CLASSES --------------------------
#-----------------------------------------------------------------------
#Each task has its own class, to execute the corresponding commands

#Enables or disables one channel of a single power supply. Called by the power supply setting page
class Check(Task):
    def __init__( self, pIndex, pDevice, pWait = False ):
        super(Check, self).__init__(pIndex, "Check", pWait)
        print("GIPHT:\tNew Check task object for " + pDevice.type + " " + pDevice.config["ID"])
        self.device    = pDevice
        self.device.taskDone.connect( self.taskFinished )

    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Check device " + self.device.config["ID"])
        self.device.check()
        #Wait until the check task is done
        while self.running and not self.device.isIdle():
            QThread.msleep(100)
        
        self.taskFinished()

class SwitchFirmware(Task):

    def __init__( self, pIndex, pFc7, pFirmware ):
        super(SwitchFirmware, self).__init__(pIndex, "SwitchFirmware", True)
        print("GIPHT:\tNew SwitchFirmware task object for " + pFc7.type + " " + pFc7.config["ID"])
        self.fc7 = pFc7
        self.firmware = pFirmware
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Load new firmware on " + self.fc7.config["ID"])
        ip = self.fc7.config["IPAddress"]
        self.askPermission.emit(self, [ "Firmware", "Switch Firmware on\nFC7 " + ip + "\nto " + self.firmware+ " ?"])
            
        while not self.permission and self.running:
            QThread.msleep(100)

        if self.running:
            self.fc7.switchFirmware(self.firmware)
            #Wait until the check task is done
            while self.running and not self.fc7.isIdle():
                QThread.msleep(100)
        self.taskFinished()

class UploadFirmware(Task):

    def __init__( self, pIndex, pFc7, pFirmware ):
        super(UploadFirmware, self).__init__(pIndex, "UploadFirmware", True)
        print("GIPHT:\tNew UploadFirmware task object for " + pFc7.type + " " + pFc7.config["ID"])
        self.fc7 = pFc7
        self.firmware = pFirmware
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Load new firmware on " + self.fc7.config["ID"])
        ip = self.fc7.config["IPAddress"]
        self.askPermission.emit(self, [ "Firmware", "Upload Firmware\n"+ self.firmware + "\nto FC7 " + ip +" ?"])
            
        while not self.permission and self.running:
            QThread.msleep(100)

        if self.running:
            self.fc7.uploadFirmware(self.firmware)
            #Wait until the check task is done
            while self.running and not self.fc7.isIdle():
                QThread.msleep(100)
        self.taskFinished()

class EnableDisableChannel(Task):
    def __init__( self, pIndex, pPowerSupply, pChannelIndex ):
        super(EnableDisableChannel, self).__init__(pIndex, "EnableDisableChannel")
        print("GIPHT:\tNew EnableDisableChannel task object")
        self.powerSupply    = pPowerSupply
        self.channelIndex   = pChannelIndex

    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Enable/Disable channel " + self.powerSupply.channels[self.channelIndex].config["ID"] + " on " +self.powerSupply.config["ID"])
        self.powerSupply.enableDisableChannel(self.channelIndex)
        while self.running and not self.powerSupply.isIdle():
            QThread.msleep(100)
        self.taskFinished()

class EnableChannels(Task):
    def __init__( self, pIndex, pPowerSupplyAndChannels, pAskForPermission = True, pWaitForTask = False ):
        super(EnableChannels, self).__init__(pIndex, "EnableChannels", pWaitForTask)
        print("GIPHT:\tNew EnableChannels task object")
        self.powerSupplyAndChannels = pPowerSupplyAndChannels
        self.display = ""
        self.askForPermission = pAskForPermission
        for powerSupplyAndChannel in self.powerSupplyAndChannels:
            self.display += "\n" + powerSupplyAndChannel[0].config["ID"] + "(" + powerSupplyAndChannel[1].config["ID"] +")"
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Enable channels " +self.display.replace("\n",""))

        if self.askForPermission:
            self.askPermission.emit(self, [ "Voltage", "Enable channels" + self.display + "?"])
                
            while not self.permission and self.running:
                QThread.msleep(100)

        if self.running:
            for powerSupplyAndChannel in self.powerSupplyAndChannels:
                powerSupplyAndChannel[0].enableChannel(powerSupplyAndChannel[1].index)

            #Only continue of all power supplies are back on idle
            while self.running and not all( [ powerSupplyAndChannel[0].isIdle() for powerSupplyAndChannel in self.powerSupplyAndChannels ] ):
                QThread.msleep(100)
        self.taskFinished()

class DisableChannels(Task):
    def __init__( self, pIndex, pPowerSupplyAndChannels, pWaitForTask = False ):
        super(DisableChannels, self).__init__(pIndex, "DisableChannels", pWaitForTask)
        print("GIPHT:\tNew DisableChannels task object")
        self.powerSupplyAndChannels = pPowerSupplyAndChannels
        self.display = ""
        for powerSupplyAndChannel in self.powerSupplyAndChannels:
            self.display += "\n" + powerSupplyAndChannel[0].config["ID"] + "(" + powerSupplyAndChannel[1].config["ID"]+")"
    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Disable channels " +self.display.replace("\n",""))

        #Permission not necessary for disabling
        #self.askPermission.emit(self, [ "Voltage", "Enable channels" + self.display + "?"])            
        #while not self.permission and self.running:
        #    QThread.msleep(100)

        if self.running:
            for powerSupplyAndChannel in self.powerSupplyAndChannels:
                powerSupplyAndChannel[0].disableChannel(powerSupplyAndChannel[1].index)

            #Only continue of all power supplies are back on idle
            while self.running and not all( [ powerSupplyAndChannel[0].isIdle() for powerSupplyAndChannel in self.powerSupplyAndChannels ] ):
                QThread.msleep(100)
        self.taskFinished()


class SetVoltages(Task):
    def __init__( self, pIndex, pPowerSuppliesChannelsVoltages, pAskForPermission = True, pWaitForTask = False ):
        super(SetVoltages, self).__init__(pIndex, "SetVoltages", pWaitForTask)
        print("GIPHT:\tNew SetVoltages task object")
        self.powerSuppliesChannelsVoltages    = pPowerSuppliesChannelsVoltages         #is a list of a list containing device, channel, voltage
        self.askForPermission = pAskForPermission
        self.display = ""
        for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
            self.display += "\n" + powerSupplyChannelVoltage[0].config["ID"] + "(" + powerSupplyChannelVoltage[1].config["ID"]+") : {:.2f}".format(powerSupplyChannelVoltage[2])


    def run( self ):
        self.running = True
        self.setProgress ( 0 )
        self.setStatus ("Running: Set voltages to channels " + self.display.replace("\n",""))

        #Check for all channels whether the voltage is in the allowed range
        if all ( [ powerSupplyChannelVoltage[1].voltageInRange( powerSupplyChannelVoltage[2]) for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ] ):
            #Dont ask for permission if the set voltage is 0
            if not all( [powerSupplyChannelVoltage[2] == 0 for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                if self.askForPermission:
                    self.askPermission.emit(self, [ "Voltage", "Apply " + self.display + "?"])            

                    while not self.permission and self.running:
                        QThread.msleep(100)

            if self.running:
                for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages:
                    powerSupplyChannelVoltage[0].setVoltage(powerSupplyChannelVoltage[1].index, powerSupplyChannelVoltage[2])
                while self.running and not all( [powerSupplyChannelVoltage[0].isIdle() for powerSupplyChannelVoltage in self.powerSuppliesChannelsVoltages ]):
                    QThread.msleep(100)
        else:
            print(self.CRED + "Voltage is not in allowed range for power supply!" + self.CEND)
        self.taskFinished()

class KIRASettings(Task):
    def __init__( self, pIndex, pDevice, pSettingsDict ):
        super(KIRASettings, self).__init__(pIndex, "KIRASettings")

        print("GIPHT:\tNew KIRASettings task object")
        self.device    = pDevice
        self.settings  = pSettingsDict
        self.keepRunning = False

    def run( self ):
        self.running = True
        self.setStatus("Running: KIRASettings " + self.device.config["ID"])
        self.setProgress ( 0 )
        if self.running:
            self.device.updateAndWriteKIRASettings(self.settings)
        self.taskFinished()

class MonitorPoll(Task):
    def __init__( self, pIndex, pDevice ):
        super(MonitorPoll, self).__init__(pIndex, "MonitorPoll")

        print("GIPHT:\tNew MonitorPoll task object")
        self.device    = pDevice
        self.keepRunning = False

    def run( self ):
        self.running = True
        self.setStatus("Running: MonitorPoll " + self.device.config["ID"])
        
        while self.running:
            diff = ( datetime.now() - self.device.lastReadout ).total_seconds() *1000
            #print(diff)
            if diff > 1000:
                self.device.monitor()
                while self.running and not self.device.isIdle():
                    QThread.msleep(100)
            QThread.msleep(1500)

        self.taskFinished()