# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'Slot.ui'
##
## Created by: Qt User Interface Compiler version 5.15.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_Slot(object):
    def setupUi(self, Slot):
        if not Slot.objectName():
            Slot.setObjectName(u"Slot")
        Slot.resize(463, 218)
        self.gridLayoutWidget = QWidget(Slot)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 441, 200))
        self.slotLayout = QGridLayout(self.gridLayoutWidget)
        self.slotLayout.setObjectName(u"slotLayout")
        self.slotLayout.setSizeConstraint(QLayout.SetMaximumSize)
        self.slotLayout.setContentsMargins(0, 0, 0, 0)
        self.idLineEdit = QLineEdit(self.gridLayoutWidget)
        self.idLineEdit.setObjectName(u"idLineEdit")

        self.slotLayout.addWidget(self.idLineEdit, 2, 0, 1, 2)

        self.lvSupplyLabel = QLabel(self.gridLayoutWidget)
        self.lvSupplyLabel.setObjectName(u"lvSupplyLabel")

        self.slotLayout.addWidget(self.lvSupplyLabel, 4, 0, 1, 1)

        self.slotNumber = QLabel(self.gridLayoutWidget)
        self.slotNumber.setObjectName(u"slotNumber")

        self.slotLayout.addWidget(self.slotNumber, 0, 1, 1, 1)

        self.slotLabel = QLabel(self.gridLayoutWidget)
        self.slotLabel.setObjectName(u"slotLabel")

        self.slotLayout.addWidget(self.slotLabel, 0, 0, 1, 1)

        self.lvSupplyComboBox = QComboBox(self.gridLayoutWidget)
        self.lvSupplyComboBox.setObjectName(u"lvSupplyComboBox")

        self.slotLayout.addWidget(self.lvSupplyComboBox, 5, 0, 1, 1)

        self.hvSupplyComboBox = QComboBox(self.gridLayoutWidget)
        self.hvSupplyComboBox.setObjectName(u"hvSupplyComboBox")

        self.slotLayout.addWidget(self.hvSupplyComboBox, 5, 1, 1, 1)

        self.randomPushButton = QPushButton(self.gridLayoutWidget)
        self.randomPushButton.setObjectName(u"randomPushButton")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.randomPushButton.sizePolicy().hasHeightForWidth())
        self.randomPushButton.setSizePolicy(sizePolicy)
        self.randomPushButton.setMaximumSize(QSize(30, 16777215))
        self.randomPushButton.setLayoutDirection(Qt.RightToLeft)

        self.slotLayout.addWidget(self.randomPushButton, 2, 2, 1, 1)

        self.removePushButton = QPushButton(self.gridLayoutWidget)
        self.removePushButton.setObjectName(u"removePushButton")
        sizePolicy.setHeightForWidth(self.removePushButton.sizePolicy().hasHeightForWidth())
        self.removePushButton.setSizePolicy(sizePolicy)
        self.removePushButton.setMaximumSize(QSize(30, 16777215))
        self.removePushButton.setLayoutDirection(Qt.RightToLeft)

        self.slotLayout.addWidget(self.removePushButton, 0, 2, 1, 1)

        self.typeLineEdit = QLineEdit(self.gridLayoutWidget)
        self.typeLineEdit.setObjectName(u"typeLineEdit")

        self.slotLayout.addWidget(self.typeLineEdit, 3, 0, 1, 1)

        self.builtLineEdit = QLineEdit(self.gridLayoutWidget)
        self.builtLineEdit.setObjectName(u"builtLineEdit")

        self.slotLayout.addWidget(self.builtLineEdit, 3, 1, 1, 1)

        self.hvSupplyLabel = QLabel(self.gridLayoutWidget)
        self.hvSupplyLabel.setObjectName(u"hvSupplyLabel")

        self.slotLayout.addWidget(self.hvSupplyLabel, 4, 1, 1, 1)

        self.arduinoLabel = QLabel(self.gridLayoutWidget)
        self.arduinoLabel.setObjectName(u"arduinoLabel")

        self.slotLayout.addWidget(self.arduinoLabel, 4, 2, 1, 1)

        self.arduinoComboBox = QComboBox(self.gridLayoutWidget)
        self.arduinoComboBox.setObjectName(u"arduinoComboBox")

        self.slotLayout.addWidget(self.arduinoComboBox, 5, 2, 1, 1)


        self.retranslateUi(Slot)

        QMetaObject.connectSlotsByName(Slot)
    # setupUi

    def retranslateUi(self, Slot):
        Slot.setWindowTitle(QCoreApplication.translate("Slot", u"Form", None))
        self.lvSupplyLabel.setText(QCoreApplication.translate("Slot", u"LV power supply", None))
        self.slotNumber.setText(QCoreApplication.translate("Slot", u"TextLabel", None))
        self.slotLabel.setText(QCoreApplication.translate("Slot", u"Slot", None))
        self.randomPushButton.setText(QCoreApplication.translate("Slot", u"R", None))
        self.removePushButton.setText(QCoreApplication.translate("Slot", u"X", None))
        self.hvSupplyLabel.setText(QCoreApplication.translate("Slot", u"HV power supply", None))
        self.arduinoLabel.setText(QCoreApplication.translate("Slot", u"Arduino", None))
    # retranslateUi
